import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:weather_app/features/weather/domain/use_cases/use_cases.dart';

import '../../../../helpers/dummy_data/entities/test_weather.dart';
import '../../../../helpers/test_helper/test_helper.mocks.dart';

void main() {
  late MockWeatherRepository mockWeatherRepository;
  late GetCurrentWeather getCurrentWeather;

  setUp(() {
    mockWeatherRepository = MockWeatherRepository();
    getCurrentWeather = GetCurrentWeather(mockWeatherRepository);
  });

  const testWeatherEntity = testWeather;

  const testCityName = 'Tampico';

  test(
    'should get current weather detail from repository',
    () async {
      when(mockWeatherRepository.getCurrentWeather(testCityName)).thenAnswer(
        (_) async => const Right(testWeatherEntity),
      );

      final response = await getCurrentWeather(testCityName);

      expect(response, equals(const Right(testWeatherEntity)));
    },
  );
}
