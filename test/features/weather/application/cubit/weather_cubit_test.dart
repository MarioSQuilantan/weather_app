import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:weather_app/core/enums/enums.dart';
import 'package:weather_app/core/error/failure.dart';
import 'package:weather_app/features/weather/application/cubits/weather_cubit.dart';
import 'package:weather_app/features/weather/domain/use_cases/use_cases.dart';

import '../../../../helpers/dummy_data/entities/test_weather.dart';
import 'weather_cubit_test.mocks.dart';

@GenerateMocks([GetCurrentWeather])
void main() {
  late MockGetCurrentWeather mockGetCurrentWeather;
  late WeatherCubit weatherCubit;

  setUp(() {
    mockGetCurrentWeather = MockGetCurrentWeather();
    weatherCubit = WeatherCubit(mockGetCurrentWeather);
  });

  const tWeather = testWeather;
  const tCityName = '';

  test(
    'initial state should be empty',
    () {
      expect(weatherCubit.state.requestStatus, RequestStatus.empty);
    },
  );

  blocTest<WeatherCubit, WeatherState>(
    'should emit [loading, success] when data is gotten successfully',
    build: () {
      when(mockGetCurrentWeather(tCityName)).thenAnswer((_) async => const Right(tWeather));
      return weatherCubit;
    },
    act: (cubit) => cubit.onGetCurrentWeather(tCityName),
    wait: const Duration(milliseconds: 100),
    expect: () => [
      const WeatherState(
        requestStatus: RequestStatus.loading,
        cityName: tCityName,
      ),
      const WeatherState(
        requestStatus: RequestStatus.success,
        weather: tWeather,
      ),
    ],
    verify: (_) {
      verify(mockGetCurrentWeather(tCityName)).called(1);
    },
  );

  blocTest<WeatherCubit, WeatherState>(
    'should emit [loading, failed] when get data is unsuccessful',
    build: () {
      when(mockGetCurrentWeather(tCityName)).thenAnswer(
        (_) async => const Left(
          ServerFailure(
            'Server failure',
          ),
        ),
      );
      return weatherCubit;
    },
    act: (cubit) => cubit.onGetCurrentWeather(tCityName),
    expect: () => [
      const WeatherState(
        requestStatus: RequestStatus.loading,
        cityName: tCityName,
      ),
      const WeatherState(
        errorMessage: 'Server failure',
        requestStatus: RequestStatus.failed,
      ),
    ],
    verify: (bloc) {
      verify(mockGetCurrentWeather(tCityName)).called(1);
    },
  );
}
