import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:weather_app/core/error/failure.dart';
import 'package:weather_app/features/weather/infrastructure/repositories/weather_repository_impl.dart';

import '../../../../helpers/dummy_data/entities/test_weather.dart';
import '../../../../helpers/test_helper/test_helper.mocks.dart';

void main() {
  late MockOpenWeatherDataSource mockOpenWeatherDataSource;
  late WeatherRepositoryImpl repository;

  setUp(() {
    mockOpenWeatherDataSource = MockOpenWeatherDataSource();
    repository = WeatherRepositoryImpl(
      dataSource: mockOpenWeatherDataSource,
    );
  });

  const tWeather = testWeather;

  group('get current weather', () {
    const tCityName = 'Jakarta';

    test(
      'should return current weather when a call to data source is successful',
      () async {
        // arrange
        when(mockOpenWeatherDataSource.getCurrentWeather(tCityName)).thenAnswer((_) async => testWeather);

        // act
        final result = await repository.getCurrentWeather(tCityName);

        // assert
        verify(mockOpenWeatherDataSource.getCurrentWeather(tCityName));
        expect(result, equals(const Right(tWeather)));
      },
    );

    test(
      'should return server failure when a call to data source is unsuccessful',
      () async {
        // arrange
        when(mockOpenWeatherDataSource.getCurrentWeather(tCityName))
            .thenThrow(DioException(type: DioExceptionType.badResponse, requestOptions: RequestOptions()));

        // act
        final result = await repository.getCurrentWeather(tCityName);

        // assert
        verify(mockOpenWeatherDataSource.getCurrentWeather(tCityName));
        expect(result, equals(const Left(ServerFailure('Oops something went wrong!'))));
      },
    );
  });
}
