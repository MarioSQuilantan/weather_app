import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:weather_app/core/constants/urls.dart';
import 'package:weather_app/core/error/exception.dart';
import 'package:weather_app/features/weather/infrastructure/data_sources/open_weather_data_source.dart';
import 'package:weather_app/features/weather/infrastructure/models/open_weather/weather_from_open_weather_model.dart';

import '../../../../helpers/json_reader.dart';
import '../../../../helpers/test_helper/test_helper.mocks.dart';

void main() {
  late MockDioClient mockDioClient;
  late OpenWeatherDataSource dataSource;

  setUp(() {
    mockDioClient = MockDioClient();
    dataSource = OpenWeatherDataSource(client: mockDioClient);
  });

  group('get current weather', () {
    const tCityName = 'Tampico';
    final responsePayload = jsonDecode(jsonReader('weather_open_weather_response.json'));
    final tWeatherModel = WeatherFromOpenWeatherModel.fromJson(
      json.decode(
        jsonReader('weather_open_weather_response.json'),
      ),
    );

    test(
      'should return a weather entity when the response code is 200',
      () async {
        when(
          mockDioClient.get(
            Urls.currentWeatherByName(tCityName),
            // data: responsePayload,
          ),
        ).thenAnswer(
          (_) async => Future.value(
            Response(
              data: responsePayload,
              statusCode: 200,
              requestOptions: RequestOptions(
                path: Urls.currentWeatherByName(tCityName),
              ),
            ),
          ),
        );

        final result = await dataSource.getCurrentWeather(tCityName);

        expect(result, equals(tWeatherModel.toEntity()));
      },
    );

    test(
      'should throw a server exception when the response code is not 200 or other',
      () async {
        // arrange
        when(
          mockDioClient.get(Urls.currentWeatherByName(tCityName)),
        ).thenAnswer(
          (_) async => Future.value(
            Response(
              data: '',
              statusCode: 404,
              requestOptions: RequestOptions(
                path: Urls.currentWeatherByName(tCityName),
              ),
            ),
          ),
        );

        // act
        final result = dataSource.getCurrentWeather(tCityName);

        // assert
        expect(() => result, throwsA(isA<ServerException>()));
      },
    );
  });
}
