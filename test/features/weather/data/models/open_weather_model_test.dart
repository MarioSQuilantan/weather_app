import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:weather_app/features/weather/infrastructure/models/open_weather/weather_from_open_weather_model.dart';

import '../../../../helpers/dummy_data/entities/test_weather.dart';
import '../../../../helpers/dummy_data/models/test_weather_from_open_weather_model.dart';
import '../../../../helpers/json_reader.dart';

void main() {
  const tWeatherModel = testWeatherModel;

  const tWeather = testWeather;

  group('toEntity', () {
    test(
      'the model should be a subclass of Weather entity',
      () async {
        // assert
        final result = tWeatherModel.toEntity();
        expect(result, equals(tWeather));
      },
    );
  });

  group('fromJson', () {
    test(
      'should return a valid model from the json data',
      () async {
        // arrange
        final Map<String, dynamic> jsonMap = json.decode(
          jsonReader('weather_open_weather_response.json'),
        );

        // act
        final result = WeatherFromOpenWeatherModel.fromJson(jsonMap);

        // assert
        expect(result, equals(tWeatherModel));
      },
    );
  });

  group('toJson', () {
    test(
      'should return a json map containing proper data',
      () async {
        // act
        final result = tWeatherModel.toJson();

        // assert
        final expectedJsonMap = {
          'weather': [
            {
              'main': '',
              'description': '',
              'icon': '',
            }
          ],
          'main': {
            'temp': 0.0,
            'pressure': 0,
            'humidity': 0,
          },
          'name': '',
        };
        expect(result, equals(expectedJsonMap));
      },
    );
  });
}
