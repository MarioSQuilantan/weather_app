import 'package:dio/dio.dart';
import 'package:mockito/annotations.dart';
import 'package:weather_app/features/weather/domain/repositories/weather_repository.dart';
import 'package:weather_app/features/weather/infrastructure/data_sources/open_weather_data_source.dart';

@GenerateMocks(
  [
    WeatherRepository,
    OpenWeatherDataSource,
  ],
  customMocks: [MockSpec<Dio>(as: #MockDioClient)],
)
void main() {}
