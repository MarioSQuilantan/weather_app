import 'package:weather_app/features/weather/domain/entities/weather.dart';

const testWeather = Weather(
  cityName: '',
  main: '',
  description: '',
  iconCode: '',
  temperature: 0.0,
  pressure: 0,
  humidity: 0,
);
