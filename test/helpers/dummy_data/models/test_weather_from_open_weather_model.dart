import 'package:weather_app/features/weather/infrastructure/models/open_weather/weather_from_open_weather_model.dart';

const testWeatherModel = WeatherFromOpenWeatherModel(
  cityName: '',
  main: '',
  description: '',
  iconCode: '',
  temperature: 0.0,
  pressure: 0,
  humidity: 0,
);
