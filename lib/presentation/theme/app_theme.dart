import 'package:flutter/material.dart';

part 'dark_theme.dart';
part 'light_theme.dart';

class AppTheme {
  static final light = lightTheme;
  static final dark = darkTheme;
}
