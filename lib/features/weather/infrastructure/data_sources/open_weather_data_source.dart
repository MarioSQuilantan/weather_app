import 'package:dio/dio.dart';
import 'package:weather_app/core/error/exception.dart';

import '../../../../core/constants/urls.dart';
import '../../domain/data_sources/weather_data_source.dart';
import '../../domain/entities/weather.dart';
import '../models/open_weather/weather_from_open_weather_model.dart';

class OpenWeatherDataSource implements WeatherDataSource {
  final Dio client;
  OpenWeatherDataSource({required this.client});
  @override
  Future<Weather> getCurrentWeather(String cityName) async {
    final response = await client.get(Urls.currentWeatherByName(cityName));
    if (response.statusCode == 200) {
      final weatherResponse = WeatherFromOpenWeatherModel.fromJson(response.data);
      return weatherResponse.toEntity();
    }
    throw ServerException();
  }
}
