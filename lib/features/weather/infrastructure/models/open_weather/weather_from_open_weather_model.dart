import 'package:equatable/equatable.dart';

import '../../../domain/entities/weather.dart';

part 'weather_from_open_weather_response.dart';

class WeatherFromOpenWeatherModel extends Equatable {
  const WeatherFromOpenWeatherModel({
    required this.cityName,
    required this.main,
    required this.description,
    required this.iconCode,
    required this.temperature,
    required this.pressure,
    required this.humidity,
  });

  final String cityName;
  final String main;
  final String description;
  final String iconCode;
  final double temperature;
  final int pressure;
  final int humidity;

  factory WeatherFromOpenWeatherModel.fromJson(Map<String, dynamic> json) => WeatherFromOpenWeatherModel(
        cityName: WeatherFromOpenWeatherResponse.fromJson(json).name,
        main: WeatherFromOpenWeatherResponse.fromJson(json).weather[0].main,
        description: WeatherFromOpenWeatherResponse.fromJson(json).weather[0].description,
        iconCode: WeatherFromOpenWeatherResponse.fromJson(json).weather[0].icon,
        temperature: WeatherFromOpenWeatherResponse.fromJson(json).main.temp,
        pressure: WeatherFromOpenWeatherResponse.fromJson(json).main.pressure,
        humidity: WeatherFromOpenWeatherResponse.fromJson(json).main.humidity,
      );

  Map<String, dynamic> toJson() => {
        'weather': [
          {
            'main': main,
            'description': description,
            'icon': iconCode,
          },
        ],
        'main': {
          'temp': temperature,
          'pressure': pressure,
          'humidity': humidity,
        },
        'name': cityName,
      };
  Weather toEntity() => Weather(
        cityName: cityName,
        main: main,
        description: description,
        iconCode: iconCode,
        temperature: temperature,
        pressure: pressure,
        humidity: humidity,
      );
  @override
  List<Object> get props => [
        cityName,
        main,
        description,
        iconCode,
        temperature,
        pressure,
        humidity,
      ];
}
