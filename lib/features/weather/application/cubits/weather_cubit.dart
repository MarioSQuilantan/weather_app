import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/core/enums/enums.dart';
import 'package:weather_app/features/weather/domain/entities/weather.dart';

import '../../domain/use_cases/use_cases.dart';

part 'weather_state.dart';

class WeatherCubit extends Cubit<WeatherState> {
  final GetCurrentWeather getCurrentWeather;
  WeatherCubit(this.getCurrentWeather) : super(const WeatherState());

  Future<void> onGetCurrentWeather(String cityName) async {
    emit(state.copyWith(
      requestStatus: RequestStatus.loading,
      cityName: cityName,
    ));
    final result = await getCurrentWeather(cityName);
    result.fold(
      (error) {
        emit(
          state.copyWith(
            requestStatus: RequestStatus.failed,
            errorMessage: error.message,
          ),
        );
      },
      (weather) {
        emit(
          state.copyWith(
            requestStatus: RequestStatus.success,
            weather: weather,
          ),
        );
      },
    );
  }
}
