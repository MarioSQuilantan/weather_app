part of 'weather_cubit.dart';

class WeatherState extends Equatable {
  final Weather? weather;
  final String errorMessage;
  final RequestStatus requestStatus;
  final String cityName;
  const WeatherState({
    this.weather,
    this.requestStatus = RequestStatus.empty,
    this.errorMessage = '',
    this.cityName = '',
  });

  WeatherState copyWith({
    Weather? weather,
    RequestStatus? requestStatus,
    String? errorMessage,
    String? cityName,
  }) =>
      WeatherState(
        weather: weather ?? this.weather,
        requestStatus: requestStatus ?? this.requestStatus,
        errorMessage: errorMessage ?? this.errorMessage,
        cityName: cityName ?? this.cityName,
      );
  @override
  List<Object?> get props => [
        weather,
        requestStatus,
        errorMessage,
        cityName,
      ];
}
