import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../entities/weather.dart';
import '../repositories/weather_repository.dart';

class GetCurrentWeather {
  final WeatherRepository repository;
  GetCurrentWeather(this.repository);
  Future<Either<Failure, Weather>> call(String cityName) => repository.getCurrentWeather(cityName);
}
