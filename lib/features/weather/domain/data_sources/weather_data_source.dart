import 'package:weather_app/features/weather/domain/entities/weather.dart';

abstract class WeatherDataSource {
  Future<Weather> getCurrentWeather(String cityName);
}
