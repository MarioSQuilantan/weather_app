import 'package:flutter/material.dart';

void showResponseErrorAlert(BuildContext context, {required String errorMessage, required Function onPressed}) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) => AlertDialog(
      title: const Text('Something went wrong'),
      content: Text(errorMessage),
      actions: [
        FilledButton(
          onPressed: () {
            dismissAlert(context);
          },
          child: const Text('Cancel'),
        ),
        FilledButton(
          onPressed: () {
            onPressed();
            dismissAlert(context);
          },
          child: const Text('Try again'),
        ),
      ],
    ),
  );
}

void dismissAlert(BuildContext context) {
  if (context.mounted) Navigator.of(context).pop();
}
