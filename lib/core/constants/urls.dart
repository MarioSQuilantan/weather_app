import 'package:weather_app/core/constants/environment.dart';

class Urls {
  static const String _baseUrl = 'https://api.openweathermap.org/data/2.5';
  static final String _apiKey = Environment.weatherApiKey;
  static String currentWeatherByName(String cityName) => '$_baseUrl/weather?q=$cityName&appid=$_apiKey';
  static String weatherIcon(String iconCode) => 'http://openweathermap.org/img/wn/$iconCode@2x.png';
}
