import 'package:go_router/go_router.dart';
import 'package:weather_app/core/routes/routes.dart';
import 'package:weather_app/presentation/screens/screens.dart';

final appRouter = GoRouter(
  routes: [
    GoRoute(
      path: Routes.home,
      builder: (_, __) => const HomeScreen(),
    ),
  ],
);
