import 'dart:async';

import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:weather_app/features/weather/application/cubits/weather_cubit.dart';
import 'package:weather_app/features/weather/domain/data_sources/weather_data_source.dart';
import 'package:weather_app/features/weather/domain/repositories/weather_repository.dart';
import 'package:weather_app/features/weather/domain/use_cases/get_current_weather.dart';
import 'package:weather_app/features/weather/infrastructure/data_sources/open_weather_data_source.dart';
import 'package:weather_app/features/weather/infrastructure/repositories/weather_repository_impl.dart';

final locator = GetIt.instance;

Future<void> init() async {
  // cubits
  locator.registerFactory(() => WeatherCubit(locator()));
  // use cases
  locator.registerLazySingleton(() => GetCurrentWeather(locator()));
  //repositories
  locator.registerLazySingleton<WeatherRepository>(() => WeatherRepositoryImpl(dataSource: locator()));
  //data sources
  locator.registerLazySingleton<WeatherDataSource>(() => OpenWeatherDataSource(client: locator()));
  // External
  locator.registerLazySingleton(() => Dio());
}
